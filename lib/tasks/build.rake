# NOTE: This is only intended to be used for local development/testing of builds, to call the same commands which
#       the various CI build jobs call.  We don't try to use these rake tasks in CI from the .gitlab-ci.yml,
#       instead we invoke the commands directly, so that build failures have simpler stacktraces
#       that aren't wrapped in the Rake stack.  It's some duplication for now, we may clean
#       it up later as the monorepo work progresses.
namespace :build do
  desc "Build the entire site, including all sub-sites"
  task :all do
    run_cmd('rm -rf public')
    run_cmd('mkdir -p public')

    # Build top level
    build_images
    build_packaged_assets
    build_assets
    build_marketing_site # Marketing uses partial_build with separate proxy resources, so it's a custom method
    build_site(:handbook)
  end

  desc "Build images"
  task :images do
    build_images
  end

  desc "Build packaged assets"
  task :packaged_assets do
    build_packaged_assets
  end

  desc "Build assets"
  task :assets do
    build_assets
  end

  desc "Build sites/marketing site"
  task :marketing do
    build_marketing_site
  end

  desc "Build sites/handbook site"
  task :handbook do
    build_site(:handbook)
  end

  private

  def middleman_build_cmd(env_vars_prefix = '')
    "#{env_vars_prefix} NO_CONTRACTS=#{ENV['NO_CONTRACTS'] || 'true'} middleman build --no-clean --bail"
  end

  def run_cmd(cmd)
    system(cmd) || raise("command failed: #{cmd}")
  end

  def build_images
    puts "\n\nBuilding images..."
    [
      "rsync -qlaP --exclude='/team/' --exclude='.gitkeep' source/images/ public/images"
    ].each do |cmd|
      run_cmd(cmd)
    end
  end

  def build_packaged_assets
    puts "\n\nBuilding packaged assets..."
    [
      'yarn install',
      'yarn run build',
      'mv tmp/frontend/javascripts public/'
    ].each do |cmd|
      run_cmd(cmd)
    end
  end

  def build_assets
    puts "\n\nBuilding assets..."
    env_vars =
      "DESTINATION_PATH_REGEXES='^ico/,^stylesheets/,^javascripts/' " \
      "MIDDLEMAN_CONFIG_FILE_NAME='config_assets.rb'"
    run_cmd(middleman_build_cmd(env_vars))
  end

  # NOTE: This is slower than just doing a `middleman build` without using PartialBuild, but
  #       this way is closer to ci and exercises the same logic in PartialBuild.
  #       If you just want to test something locally, use development mode.  If you want to
  #       test part of a build locally, manually invoke the command with the correct CI_NODE_INDEX
  def build_marketing_site
    puts "\n\nBuilding marketing site from sites/marketing..."

    FileUtils.cd('sites/marketing') do
      puts "\n\nBuilding marketing site PartialBuild for proxy resources..."
      run_cmd(middleman_build_cmd("CI_NODE_TOTAL=5 CI_BUILD_PROXY_RESOURCE=true INCLUDE_GENERATORS=true"))

      (1..5).each do |ci_node_index|
        puts "\n\nBuilding marketing site PartialBuild for CI_NODE_INDEX=#{ci_node_index}..."
        run_cmd(middleman_build_cmd("CI_NODE_TOTAL=5 CI_NODE_INDEX=#{ci_node_index}"))
      end
    end
  end

  def build_site(site)
    site_dir = File.expand_path("../../sites/#{site}", __dir__)
    Dir.chdir(site_dir) do
      puts "\n\nBuilding '#{site}' site from #{site_dir}..."
      system(middleman_build_cmd) || raise("command failed for '#{site}' site in #{site_dir}: #{middleman_build_cmd}")
    end
  end
end
